// const axios = require('axios')
// const url = 'http://checkip.amazonaws.com/';
let response;

exports.handler = async (event, context) => {
    const now = new Date(Date.now());
    console.log({ execution: now.toISOString() });
    try {
        // const ret = await axios(url);
        response = {
            'statusCode': 200,
            'body': JSON.stringify({
                message: 'hello world',
                // location: ret.data.trim()
            })
        }
    } catch (err) {
        console.log(err);
        return err;
    }

    return response
};
